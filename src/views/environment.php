<?php
    echo "<h2>Configuration Environment</h2><br>";
    if (isset($_SESSION['decidir_message'])) {
        echo $_SESSION['decidir_message'];
        session_destroy();
    }
?>
<form id="keysForm" name="keysForm" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST">
    <fieldset class="field-group">
        <label for="public_key">Public key: </label>
        <input type="text" id="decidir_public_key" name="decidir_public_key" value="<?php echo get_option('decidir_public_key'); ?>"/>
        <br>
        <br>
        <label for="private_key">Private key: </label>
        <input type="text" id="decidir_private_key" name="decidir_private_key" value="<?php echo get_option('decidir_private_key'); ?>"/>
    </fieldset>
    <br>
    <fieldset class="field-group">
        <input type="checkbox" id="decidir_environment" name="decidir_environment" <?php echo (empty(get_option('decidir_environment'))) ? '' : 'checked';?> /><label>Prod Enviroment</label>
    <fieldset>
    <br>
    <input type="hidden" name="action" value="keysForm">
    <input type="submit" value="Save" />
</form>

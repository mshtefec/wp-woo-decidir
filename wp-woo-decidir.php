<?php
/**
 * @version 1.0.0
 */

/*
Plugin Name: Crombie Decidir Pasarela de Pagos
Plugin URI: http://wordpress.org/plugins/wp-woo-decidir/
Description: This is not just a plugin, it a woocommerce integration with decidir.
Author: Crombie
Version: 1.0.0
Author URI: https://crombie.dev
License: GPL2
text-domain: decidir-pay-woo
*/

include "vendor/autoload.php";

use Decidir\Payment;

const AmbientApi = "test";

register_activation_hook(__FILE__, 'addCredentials');
register_deactivation_hook(__FILE__, 'removeCredentials');

function add_decidir_stylesheet() {
    wp_enqueue_style( 'decidir-styles', plugins_url( '"/assets/css/decidir.css"', __FILE__ ) );
}
add_action('admin_print_styles', 'add_decidir_stylesheet');

if (!session_id()) {
    session_start();
}

/* Integración con Woocommerce pagos */
if (! in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) return;

add_action('plugins_loaded', 'decidir_payment_init', 11);

add_action( 'wp_enqueue_scripts', 'add_decidir_stylesheet' );

function decidir_payment_init() {
    if (class_exists('WC_Payment_Gateway')) {
        class WC_Decidir_Pay_Gateway extends WC_Payment_Gateway {
            
            /**
             * Constructor de la clase WC_Decidir_Pay_Gateway
             */
            public function __construct() {
                $this->id = 'decidir_payment';
                $this->icon = plugin_dir_url( __FILE__ ) . 'assets/img/logo-decidir.png';
                $this->has_fields = true;
                $this->method_title = __('Pagos con DECIDIR', 'decidir-pay-woo');
                $this->method_description = __('Implementa la Plataforma de Pagos DECIDIR.', 'decidir-pay-woo');
                $this->title = $this->get_option('title');
                $this->title = $this->get_option('description');
                
                $this->init_form_fields();
                $this->init_settings();

                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            }

            /**
             * Inicializa los campos de configuracion de la forma de pago
             */
            public function init_form_fields() {
                $this->form_fields = apply_filters(
                    'woo_decidir_pay_fields', array(
                        'enabled' => array(
                            'title' => __('Enable/Disable', 'decidir-pay-woo'),
                            'type' => 'checkbox',
                            'label' => __('Enabled or Disabled DECIDIR Payment method'),
                            'default' => 'no'
                        ),
                        'title' => array(
                            'title'       => __( 'Title', 'decidir-pay-woo' ),
                            'type'        => 'text',
                            'description' => __( 'Visible title of this payment method.', 'decidir-pay-woo' ),
                            'default'     => __( 'Pay via DECIDIR', 'decidir-pay-woo' ),
                            'desc_tip'    => true,
                        ),
                
                        'description' => array(
                            'title'       => __( 'Description', 'decidir-pay-woo' ),
                            'type'        => 'textarea',
                            'description' => __( 'Visible description of this payment method.', 'decidir-pay-woo' ),
                            'default'     => __( 'Usar DECIDIR Prisma como medio de pago.', 'decidir-pay-woo' ),
                            'desc_tip'    => true,
                        ),
                    )
                );
            }
             
            /**
             *  Formulario de pago
             */
            public function payment_fields() {
                // descripciones previas al formulario de pago
                if ( $this->description ) {
                    if ( empty(get_option('decidir_environment')) ) {
                        $this->description .= ' TEST MODE ENABLED. In test mode, you can use the card numbers listed in <a href="#">documentation</a>.';
                        $this->description  = trim( $this->description );
                    }
                    // muestra la descripcion con tags <p>.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
                // imprime el formulario
                echo '<fieldset id="wc-' . esc_attr( $this->id ) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent;">';
                // Se agrega este action hook si queres que una pasarela de pago personalizada lo soporte
                do_action( 'woocommerce_credit_card_form_start', $this->id );
            
                echo '
                    <p class="form-row form-row-wide">
                        <label class="">Nombre y Apellido como figura en la tarjeta <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="card_fullname" class="input-text" id="card_fullname" type="text" autocomplete="on" placeholder="Nombre y Apellido">
                        </span>
                    </p>
                    <p class="form-row form-row-wide">
                        <label class="">Número de Tarjeta <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="card_number" class="input-text" id="card_number" type="text" autocomplete="on">
                        </span>
                    </p>
                    <div class="form-row form-row-first">
                        <label class="">Mes de Vencimiento <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="card_exp_month" class="input-text" id="card_exp_month" type="text" autocomplete="on" placeholder="MM">
                        </span>
                        <br>
                        <label class="">Tipo de Doc. <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="personal_id_type" class="input-text" id="personal_id_type" type="text" autocomplete="on" placeholder="DNI, LC, LE">
                        </span>
                        <br>
                        <label class="">Cód. de Seguridad (CVV) <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="card_cvv" class="input-text" id="card_cvv" type="password" autocomplete="on" placeholder="CVV">
                        </span>
                    </div>
                    <div class="form-row form-row-last">
                        <label class="">Año de Vencimiento <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="card_exp_year" class="input-text" id="card_exp_year" type="text" autocomplete="on" placeholder="YY">
                        </span>
                        <br>
                        <label class="">Número de Doc. <abbr class="required" title="obligatorio">*</abbr></label>
                        <span class="woocommerce-input-wrapper">
                            <input name="personal_id" class="input-text" id="personal_id" type="text" autocomplete="on" placeholder="Numero de documento">
                        </span>
                    </div>
                ';
            
                do_action( 'woocommerce_credit_card_form_end', $this->id );
            
                echo '<div class="clear"></div></fieldset>';    
                                    
            }

            /**
             * Validacion de los campos del formulario de pago
             */
            public function validate_fields() {
                if( empty( $_POST[ 'billing_first_name' ]) ) {
                    wc_add_notice(  'First name is required!', 'error' );
                    return false;
                }
                return true;        
            }

            /**
             * Procesa el pago
             */
            public function process_payment($order_id) {
                global $woocommerce, $order;
                
                $keys_data = array(
                    'decidir_public_key' => get_option('decidir_public_key'),
                    'private_key' => get_option('private_key')
                );
            
                $connector = new \Decidir\Connector($keys_data, AmbientApi);
                
                // se obtienen los detalles del pedido actual
                $order = new WC_Order( $order_id );

                // $order es un json, lo tengo que castear a array.
                $order_data = json_decode($order);
                
                // se actualiza el estado del pedido
                $order->update_status('on-hold', __('Waiting for payment process', 'decidir-pay-woo'));

                $body_token = array(
                    "card_number" => $_POST[ 'card_number' ],
                    "card_expiration_month" => $_POST["card_exp_month"],
                    "card_expiration_year" => $_POST["card_exp_year"],
                    "security_code" => $_POST["card_cvv"],
                    "card_holder_name" => $_POST["card_fullname"],
                    "card_holder_identification" => array(
                      "type" => $_POST["personal_id_type"],
                      "number" => $_POST["personal_id"]
                    )
                );

                $body_token_json = json_encode($body_token);

                // pedido del token de pago.
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://developers.decidir.com/api/v2/tokens",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_token_json,
                    CURLOPT_HTTPHEADER => array(
                        "apikey: " . get_option('decidir_public_key'),
                        "cache-control: no-cache",
                        "content-type: application/json"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                // $response es un json, lo tengo que castear a array.
                $res = json_decode($response);

                $body_payment = array(
                    "site_transaction_id" => strval($order_data->id),
                    "token" => $res->id,
                    "payment_method_id" => 1, // id identificatorio de decidir.
                    "bin" => substr($_POST[ 'card_number' ], 0, 6), // los primeros 6 números de la tarjeta.
                    "amount" => intval($order_data->total),
                    "currency" => "ARS",
                    "installments" => 1,
                    "description" => "",
                    "payment_type" => "single",
                    "sub_payments" => array()
                );

                $body_payment_json = json_encode($body_payment);

                // ejecución real del pago.
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://developers.decidir.com/api/v2/payments",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_payment_json,
                    CURLOPT_HTTPHEADER => array(
                        "apikey: " . get_option('decidir_private_key'),
                        "cache-control: no-cache",
                        "content-type: application/json"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                // $response es un json, lo tengo que castear a array.
                $res = json_decode($response);

                // si el estado esta aprobado, se guarda la orden y el pedido.
                if ( $res->status == 'approved') {
        
                    // se recive el pago
                    $order->payment_complete();
                    $order->reduce_order_stock();
            
                    // notas que se agregan al pedido
                    $order->add_order_note( __('Hey, your order is paid! Thank you!') , true );
            
                    // se vacia el carrito
                    $woocommerce->cart->empty_cart();
            
                    // se redirige a la pag de agradecimiento
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url( $order )
                    );
        
                } else {
                    wc_add_notice(  'Please try again.', 'error' );
                    return;
                }

            }

        }
    }
}

add_filter('woocommerce_payment_gateways', 'add_to_woo_decidir_payment_gateway');

function add_to_woo_decidir_payment_gateway($gateways) {
    $gateways[] = 'WC_Decidir_Pay_Gateway';
    return $gateways;
}

add_action('admin_menu', 'generateMenu');

/*
******** Genero los Menu para la Administración del plugin
*/
function generateMenu() {
    add_menu_page(
        'Gateway Woo + Decidir',
        'Woo + Decidir',
        'manage_options',
        'sp_menu',
        'bodyMenu',
        null, // ico
        '1'
    );

    add_submenu_page(
        'sp_menu',
        'Ajustes',
        'Environment',
        'manage_options',
        //'sp_menu_environment',
        plugin_dir_path(__FILE__) . 'src/views/environment.php',
        null
    );
}



function bodyMenu() {
    echo "<h2>Release Notes</h2>";
}

/*
******** Fin Generación Menu
*/

/* Guardo credenciales de decidir en wp-options si activo el plugin */
function addCredentials() {
    add_option('decidir_public_key', '96e7f0d36a0648fb9a8dcb50ac06d260', '', 'yes');
    add_option('decidir_private_key', '1b19bb47507c4a259ca22c12f78e881f', '', 'yes');
    add_option('decidir_environment', 0, '', 'yes');
    //health_check();
}

/* Elimino credenciales de decidir en wp-options si desactivo el plugin */
function removeCredentials() {
    delete_option('decidir_public_key');
    delete_option('decidir_private_key');
    delete_option('decidir_environment', 0, '', 'yes');
}

/* Actualiza las credenciales de decidir en wp-options y el entorno de ejecucion*/
function updateCredentials($decidir_public_key, $decidir_private_key, $decidir_environment) {
    update_option('decidir_public_key', $decidir_public_key, '', 'yes');
    update_option('decidir_private_key', $decidir_private_key, '', 'yes');
    update_option('decidir_environment', $decidir_environment, '', 'yes');    
    //health_check();
}

/* Simplemente comprueba el estado de la app decidir */
function health_check() {

    $keys_data = array(
        'decidir_public_key' => get_option('decidir_public_key'),
        'private_key' => get_option('private_key')
    );

    $connector = new \Decidir\Connector($keys_data, AmbientApi);

    $response = $connector->healthcheck()->getStatus();
    $response->getName();
    $response->getVersion();
    $response->getBuildTime();

    echo $response->toString();
}

/* Listado de Pagos */
function payments_list() {

    $keys_data = array(
        'decidir_public_key' => get_option('decidir_public_key'),
        'private_key' => get_option('private_key')
    );

    $connector = new \Decidir\Connector($keys_data, AmbientApi);

    $data = array("pageSize" => 5); // 5 por página

    $response = $connector->payment()->PaymentList($data);
    $response->getLimit();
    $response->getOffset();
    $response->getResults();
    $response->getHas_more();

    echo $response->toString();
}

/* Información de un Pago */
function payments_info($id) {

    $keys_data = array(
        'decidir_public_key' => get_option('decidir_public_key'),
        'private_key' => get_option('private_key')
    );

    $connector = new \Decidir\Connector($keys_data, AmbientApi);

    $data = array();

    $response = $connector->payment()->PaymentInfo($data, $id);
    $response->getId();
    $response->getSiteTransaction_id();
    $response->getToken();
    $response->getUser_id();
    $response->getPayment_method_id();
    $response->getCard_brand();
    $response->getBin();
    $response->getAmount();
    $response->getCurrency();
    $response->getInstallments();
    $response->getPayment_type();
    $response->getSub_payments();
    $response->getStatus();
    $response->getStatus_details();
    $response->getDate();
    $response->getEstablishment_name();
    $response->getFraud_detection();
    $response->getAggregate_data();
    $response->getSite_id();
}

/* Solicitud de Token de Pago */

function payments_get_token($apikey) {
    $request = new WP_REST_Request( 'GET', 'https://developers.decidir.com/api/v2/tokens/' );
    $request->set_header( 'Content-Type', 'application/json' );
    $request->set_header('apikey', $apikey);
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $data = $server->response_to_data( $response, false );
    $json = wp_json_encode( $data );

    return $json;
}

/* Anulación / Devolución Total del Pago, 'NO Incluye' borrado */
function payment_annulment() {
    
}

/* Anulación / Devolución Total del Pago, 'Incluye' borrado */
function payment_deleted() {
    
}

//add_action('woocommerce_checkout_order_processed', 'decidir_payment_form', 10, 1);

/** 
 * Carga las acciones admin_post_ para que cuando se invoquen desde el formulario keysForm 
 * llamen a la funcion wp_decidir_save_keys
*/
add_action( 'admin_post_keysForm', 'wp_decidir_save_keys' );
add_action( 'admin_post_nopriv_keysForm', 'wp_decidir_save_keys' );

/* Modifica los valores de las apikeys y del entorno en la BD */
function wp_decidir_save_keys() {
    $decidir_public_key = $_POST['decidir_public_key'];
    $decidir_private_key = $_POST['decidir_private_key'];
    if (
        !empty($decidir_public_key) &&
        !empty($decidir_private_key)
    ) {
        $decidir_environment = get_option('decidir_environment');
        if (isset($_POST['decidir_environment'])) {
            $decidir_environment = 1;
        } else {
            $decidir_environment = 0;
        }

        updateCredentials(
            $decidir_public_key, 
            $decidir_private_key, 
            $decidir_environment
        );
        $_SESSION['decidir_message'] = wp_decidir_message("Los cambios se guardaron con éxito.", "success");    
        wp_redirect( $_SERVER["HTTP_REFERER"], 302, 'WordPress' );
        exit;
    } 
    else {
        $_SESSION['decidir_message'] = wp_decidir_message("Las credenciales no pueden estar vacias.", "error");
        wp_redirect( $_SERVER["HTTP_REFERER"], 302, 'WordPress' );
        exit;
    }
}

/**
 * Muestra el mensaje que recibe y del tipo que se le diga: 
 * info, success, warning, error.
 */
function wp_decidir_message($message, $type) {
    return "<div class='decidir-message decidir-". $type . "'>" . $message . "</div>";
}